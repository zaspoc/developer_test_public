DROP TABLE IF EXISTS `test`.`trade`, `test`.`market`, `test`.`product`;

CREATE TABLE `test`.`market` ( 
    `id` INT NOT NULL , 
    `name` VARCHAR(50) NOT NULL DEFAULT '' , 
    PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;

CREATE TABLE `test`.`product` ( 
    `id` INT NOT NULL , 
    `name` VARCHAR(50) NOT NULL DEFAULT '' , 
    PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;

CREATE TABLE `test`.`trade` ( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `market_id` INT NOT NULL ,
    `product_id` INT NOT NULL ,  
    `price` DECIMAL NOT NULL DEFAULT '0' , 
    `volume` DECIMAL NOT NULL DEFAULT '0' , 
    PRIMARY KEY (`id`),
    FOREIGN KEY fk_market(market_id) REFERENCES `test`.`market`(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY fk_product(product_id) REFERENCES `test`.`product`(id) ON UPDATE CASCADE ON DELETE CASCADE
    ) ENGINE = InnoDB; 

INSERT INTO `test`.`market` (`id`, `name`) VALUES (1, 'FRANCE'), (2, 'GERMANY');
INSERT INTO `test`.`product` (`id`, `name`) VALUES (1, 'PROD01'), (2, 'PROD02'), (3, 'PROD03');
INSERT INTO `test`.`trade` (`market_id`, `product_id`, `price`, `volume`) VALUES
    (1, 1, 10.0, 50.0),
    (1, 2, 20.0, 20.0),
    (1, 3, 25.0, 35.0),
    (2, 1, 50.0, 50.0),
    (2, 2, 30.0, 30.0),
    (2, 3, 22.0, 45.0);
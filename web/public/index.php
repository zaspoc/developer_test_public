<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>EPEX SPOT Developer test</title>
    </head>
    <body>
        <h1>EPEX SPOT Developer test</h1>
        
        <h2>PHP process</h2>
        <?php

        /**
         * Only purpose is to give an example of PHP processing
         */

        include '../app/vendor/autoload.php';

        $foo = new App\Foo();

        foreach ($foo->getTrades() as $trade) {
            print_r($trade);
            echo '<br>';
        }

        ?>

        <h2>PHPUnit</h2>
        <a href="/report/phpunit/index.html">PHPUnit report</a>
    </body>
</html>

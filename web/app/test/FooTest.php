<?php

namespace App\Test;

use App\Foo;
use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    /**
     * Only purpose is to give an example for unit testing
     */
    public function testGetName()
    {
        $foo = new Foo();
        $this->assertEquals($foo->getName(), 'EPEX SPOT Developer test');
    }
}

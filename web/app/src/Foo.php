<?php

namespace App;

use \PDO;

class Foo
{
    /**
     * Only purpose is to give an example for unit testing
     */
    public function getName()
    {
        return 'EPEX SPOT Developer test';
    }

    /**
     * Only purpose is to give an example for PDO connection and fectching
     */
    public function getTrades()
    {
        try {
            $dsn = 'mysql:host=mysql;dbname=test;charset=utf8;port=3306';
            $pdo = new PDO($dsn, 'dev', 'dev');
            $stmt = $pdo->query("SELECT * FROM trade");
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

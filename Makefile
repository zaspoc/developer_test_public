# Makefile for Docker Nginx PHP Composer MySQL

include .env

# MySQL
MYSQL_INIT_DIR=etc/mysql

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  start				Create and start containers"
	@echo "  stop				Stop and clear all services"
	@echo "  mysql-init			Init the mysql database with dummy data"
	@echo "  composer-up			Update PHP dependencies with composer"
	@echo "  logs				Follow log output"
	@echo "  code-sniff			Check the API with PHP Code Sniffer (PSR2)"
	@echo "  phpmd				Analyse the API with PHP Mess Detector"
	@echo "  test				Test application"

init:
	@$(shell cp -n $(shell pwd)/web/app/composer.json.dist $(shell pwd)/web/app/composer.json 2> /dev/null)

clean:
	@rm -Rf data/
	@rm -Rf web/app/vendor
	@rm -Rf web/app/composer.lock
	@rm -Rf web/app/doc
	@rm -Rf web/public/report

code-sniff:
	@echo "Checking the standard code..."
	@docker-compose exec -T php ./app/vendor/bin/phpcs -v --standard=PSR2 app/src

composer-up:
	@docker run --rm -v $(shell pwd)/web/app:/app composer update

start: init
	docker-compose up -d

stop:
	@docker-compose down -v
	@make clean

logs:
	@docker-compose logs -f

mysql-init:
	@docker exec -i $(shell docker-compose ps -q mysqldb) mysql -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" < $(MYSQL_INIT_DIR)/init.sql 2>/dev/null

phpmd:
	@docker-compose exec -T php \
	./app/vendor/bin/phpmd \
	./app/src \
	text cleancode,codesize,controversial,design,naming,unusedcode

test: code-sniff
	@docker-compose exec -T php ./app/vendor/bin/phpunit --colors=always --configuration ./app/

.PHONY: clean test code-sniff init

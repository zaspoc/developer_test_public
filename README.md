#  EPEX SPOT developer test

## __Quick start__

__Start the application stack__
```sh
make start
```
Or if `make` is not available:
```sh
cp web/app/composer.json.dist web/app/composer.json
```
```sh
docker-compose up -d
```

__Init mysql database with dummy data__
```sh
make mysql-init
```
Or if `make` is not available:
```sh
docker exec -i mysql mysql -udev -pdev < etc/mysql/init.sql
```

__Open your favorite browser__ :
* Index of the application [http://localhost:8000](http://localhost:8000/)
* PHPMyAdmin (hostname: mysqldb, username: dev, password: dev) [http://localhost:8082](http://localhost:8082/) 

## __What has to be done__

The objective is to implement the generation of an XML file, validating the [document.xsd](http://localhost:8000/document.xsd) structure.
You can find an example of expected output here: [resultSample.xml](http://localhost:8000/resultSample.xml).

__Simple specification of the XML document:__

For each row in the `test.market` table, create a `Market` node with a `name` attribute. This node will contain the following sub-nodes:
* NumberOfTrades: contains the number of `test.trade` rows linked to this market
* WeightedAvg: contains the weighted average price (weighted by volume) of `test.trade` rows linked to this market
* TotalVolume: contains the sum of volumes of `test.trade` rows linked to this market

__Some guidance__
1. Create a local git branch to start your solution implementation
2. Organize your code the way you want in the `src/` folder. You already have a sample class `Foo.php` that contains a PDO connection example.
3. Unit tests should be located in the `test/` folder. You already have a sample class `FooTest.php`.
4. To ease the work, the `public/index.php` could trigger the XML file generation.
5. Lot of tools are available in this docker stack, i.e. trigger code validation, unit test suite, update composer dependencies... You can find all these in the following annex if required.

___

## Annex : Help manual of this docker stack

### Images used

* [Nginx](https://hub.docker.com/_/nginx/)
* [MySQL](https://hub.docker.com/_/mysql/)
* [PHP-FPM](https://hub.docker.com/r/nanoninja/php-fpm/)
* [Composer](https://hub.docker.com/_/composer/)
* [PHPMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)

This docker stack uses the following ports :

| Server     | Port |
|------------|------|
| MySQL      | 8989 |
| PHPMyAdmin | 8082 |
| Nginx      | 8000 |


### Project tree

```sh
.
├── docker-compose.yml
├── etc
│   ├── mysql
│   │   └── init.sql
│   ├── nginx
│   │   ├── default.conf
│   │   └── default.template.conf
│   └── php
│       └── php.ini
├── Makefile
├── README.md
└── web
    ├── app
    │   ├── composer.json.dist
    │   ├── phpunit.xml.dist
    │   ├── src
    │   │   └── Foo.php
    │   └── test
    │       ├── bootstrap.php
    │       └── FooTest.php
    └── public
        ├── document.xsd
        ├── index.php
        └── resultSample.xml

```


### Run the application

1. Copying the composer configuration file : 

    ```sh
    cp web/app/composer.json.dist web/app/composer.json
    ```

2. Start the application :

    ```sh
    docker-compose up -d
    ```

    **Please wait this might take a several minutes...**

    ```sh
    docker-compose logs -f # Follow log output
    ```

3. Init mysql database with dummy data

    ```sh
    docker exec -i mysql mysql -udev -pdev < etc/mysql/init.sql
    ```

4. Open your favorite browser :

    * [http://localhost:8000](http://localhost:8000/)
    * [http://localhost:8082](http://localhost:8082/) PHPMyAdmin (hostname: mysqldb, username: dev, password: dev)

5. Stop and clear services

    ```sh
    docker-compose down -v
    ```


### Use Makefile

When developing, you can use [Makefile](https://en.wikipedia.org/wiki/Make_(software)) for doing the following operations :

| Name          | Description                                    |
|---------------|------------------------------------------------|
| start         | Create and start containers                    |
| stop          | Stop and clear all services                    |
| mysql-init    | Init the mysql database with dummy data        |
| composer-up   | Update PHP dependencies with composer          |
| logs          | Follow log output                              |
| code-sniff    | Check the code with PHP Code Sniffer (`PSR2`)  |
| phpmd         | Analyse the code with PHP Mess Detector        |
| test          | Test application with phpunit                  |

__Examples__

Start the application :

```sh
make start
```

Init the mysql database with dummy data :
```sh
make mysql-init
```

### Use Docker commands

__Installing package with composer__

```sh
docker run --rm -v $(pwd)/web/app:/app composer require symfony/yaml
```

__Updating PHP dependencies with composer__

```sh
docker run --rm -v $(pwd)/web/app:/app composer update
```

__Testing PHP application with PHPUnit__

```sh
docker-compose exec -T php ./app/vendor/bin/phpunit --colors=always --configuration ./app
```

__Fixing standard code with [PSR2](http://www.php-fig.org/psr/psr-2/)__

```sh
docker-compose exec -T php ./app/vendor/bin/phpcbf -v --standard=PSR2 ./app/src
```

__Checking the standard code with [PSR2](http://www.php-fig.org/psr/psr-2/)__

```sh
docker-compose exec -T php ./app/vendor/bin/phpcs -v --standard=PSR2 ./app/src
```

__Analyzing source code with [PHP Mess Detector](https://phpmd.org/)__

```sh
docker-compose exec -T php ./app/vendor/bin/phpmd ./app/src text cleancode,codesize,controversial,design,naming,unusedcode
```

__Checking installed PHP extensions__

```sh
docker-compose exec php php -m
```

### Handling database

__MySQL shell access__

```sh
docker exec -it mysql bash
```

and

```sh
mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD"
```
___